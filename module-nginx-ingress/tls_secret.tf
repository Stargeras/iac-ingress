resource "kubernetes_secret" "tls-secret" {
  count            = var.use_nginx ? 1 : 0
  type = "kubernetes.io/tls"

  metadata {
    name      = "ingress-tls"
    namespace = "default"
  }
  data = {
    "tls.crt" = var.tls_crt
    "tls.key" = var.tls_key
  }
}
